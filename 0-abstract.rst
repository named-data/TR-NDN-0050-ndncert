.. environment:: abstract

    Certificates are widely used for different purpose in Named Data Networking (NDN).
    Each data packet is secured by its signature, which ensure the integrity and enable the authentication of the data producer.
    In NDN, every entity is supposed to have a corresponding identity (namespace) and a corresponding certificate for this namespace; thus the entity could produce data packets signed by the corresponding certificate.
    To facilitate the certificate issuing and management process, especially certificate authority's verifying an applicant's legality for the certificate, NDN certificate management protocol (NDNCERT) is designed.
    NDNCERT provides flexible and automatic certificate issuing process between certificate authority(CA) and certificate requester.
    The protocol also enable certificate owner to manage one's own identities and issue sub-namespace certificate.
    NDNCERT does not impose any specific trust model or trust anchors.
    While the primary use case of the developed protocol is to manage NDN testbed certificates, it can be used with any other set of global and local trust anchors.
