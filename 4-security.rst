Security Consideration
----------------------

Threat Model
~~~~~~~~~~~~

As a service in ndn, the protocol's threat model is different from the
Interest threat model. There are two "channels" for a ndn cert management
CA to interact with outside.

* The interest-data channel between CA and requester.
* A challenge validation channel to validate the requester's identity out of
   band.

   .. aafig::
       :aspect: 70
       :scale: 100

       +-----------+      "Interest"       +--------+  "Query"    +-----------+
       |           | --------------------> |        | ..........> |           |
       |"requester"|        "Data"         |  "CA"  |  "Result"   |"Challenge"|
       |           | <-------------------- |        | <.......... |           |
       +-----------+                       +--------+             +-----------+

The protocol aims to be secure against all kinds of attacks on any
channel. The protocol needs to protect from the network layer attacks
also the application layer attacks. We need to account for

* Man-in-the-middle attack
* Denial-of-service attack

The Interest and Data exchange safety is ensured by NDN trust schema.
Notice that the interest-data exchange can protect the information from
interception because the requester can check the name of data packet to
ensure that interest is not tampered.

The channel is safe when:

* requester builds up prior trust to CA

  Prior trust is of vital importance to defense MitM attack. Once build up the trust directly between requester and CA, there is no chance for third party MitM attack.

* certificate requester side strictly verifies the signature
* certificate requester side strictly check the Data name with the interest name
* CA side strictly verifies the signature and certificate request

For the out-of-band challenge validation channel, it is CA server's
responsibility to design and build up a safe environment.

Denial of service consideration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As a certificate service running over NDN, requests from requester will
require CA server to perform a few potentially expensive operations. To
mitigate the attacks, CA servers can take the similar strategy as that
in TCP/IP network. CA servers can take a proper rate limits. Also CA
servers can strictly check the interest request component. An interest
request without encryption or without a valid certificate request can
be ignored.

MitM consideration: Trust CA before certificate management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Requester and well-known CA

  Requester must have prior trust of Root CA certificate

  - Root CA certificate can be pre-installed to requester's key chain.
  - Root CA certificate can be installed to certificate requester manually.

* Requester and sub-namespace CA

  It is requester's responsibility to ensure that sub-namespace requester must have prior trust of CA certificate

  - NDNCERT provides a mechanism for sub-namespace requester to get sub-namespace certificate

      1. requester should indicate a trust repository to fetch CA certificate. e.g. a trust NDNS or a private repo. The default trust repository is NDN testbed root CA.
      2. The certificate requester will send an interest with the sub namespace and link object to fetch the sub-namespace certificate.
      3. Verify the certificate and install.

  - Sub-namespace CA certificate can be installed to sub-namespace requester manually.

Signature and Signature Validation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Signed Interest

  - :ndn:`_SELECT` interest command from requester must be signed with the ``requester certificate key``.
  - :ndn:`_VALIDATE` interest command from requester must be signed with the ``requester certificate key``.
  - :ndn:`_STATUS` interest command from requester must be signed with the ``requester certificate key``.

* Signed Data

  All the data from CA must be signed with the related upper level certificate.

  For example:

   ::

       For the command _NEW to apply for /ndn/edu/ucla/cs/zhiyi,
         the data should be signed by certificate of the namespace /ndn/edu/ucla/cs.
       For the command _PROBE to ask for available namespace under /ndn/edu/ucla/remap,
         the data should be signed by certificate of the namespace /ndn/edu/ucla/remap.
       For the command `_VALIDATE` to check status of issuing certificate for /ndn/edu/ucla/cs/zhiyi,
         the data should be signed by certificate of the namespace /ndn/edu/ucla/cs.

* Signature validation

  All the signed interests and all the data packets must be verified. If signature is invalid, the packet must not be used.

Encryption and Decryption
~~~~~~~~~~~~~~~~~~~~~~~~~

Encryption and decryption can be added by CA. Encryption may mainly apply to the safe challenge information part in :ndn:`_VALIDATE` interest.