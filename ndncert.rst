NDN Certificate Management Protocol
===================================

.. include:: 0-abstract.rst

.. include:: 1-intro.rst

.. include:: 2-protocol-overview.rst

.. include:: 3-formats.rst

.. include:: 4-security.rst

.. include:: 5-challenges.rst

.. include:: 6-examples.rst

Acknowledgement
===============

This work is partially supported by the
`National Science Foundation <https://www.nsf.gov>`__ under awards CNS-1345318 and CNS-1629922.
