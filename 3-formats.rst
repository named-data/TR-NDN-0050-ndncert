Packet Formats
--------------

The packet formats in NDNCERT.

Assign namespace packet exchange (optional)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_PROBE interest packet
^^^^^^^^^^^^^^^^^^^^^^

:Parameters:
  - Command Type :ndn:`_PROBE`
  - Probe information

:MustBeFresh: must be set

:Signature: No requirements

:Verification: No requirements

:Naming convention: The interest name format is as follow:
  ::

       /CA-prefix/_PROBE/<Probe Information>
                 \      /\                 /
                  -—  --  -------  --------
                    \/           \/
            Command Type     User provides a name or a email address

:Example:
   - If /ndn/edu/ucla/ CA module requires user to use email address for
     probe, a PROBE interest can be::

         /ndn/edu/ucla/_PROBE/zhiyi@cs.ucla.edu

   - If /ndn/edu/ucla/cs/zhiyi CA module requires user to use a plain
     string for probe::

         /ndn/edu/ucla/cs/zhiyi/_PROBE/my-macbook

Data packet answering _PROBE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Verification: When received, several parts need to be verified to ensure authentication:
   - The data name is the same with the interest sent
   - CA signature must be verified

:Data structure:

   .. aafig::
                                 :aspect: 70
                                 :scale: 100

                                 +--------------------------+
                                 |         "Content"        |
                                 |+------------------------+|
                                 ||       "JSON File"      ||
                                 |+------------------------+|
                                 +--------------------------+
                                 |      "SignatureInfo"     |
                                 +--------------------------+
                                 |      "SignatureValue"    |
                                 +--------------------------+

:Content format: JSON file format:
   - Identifier: The namespace assigned by CA to certificate requester based on the hint information
   - CA-info: A link to where CA publish its statement and policy

:JSON example: The example is as follow
  ::

        Example #1
        {
          "identifier": "/ndn/edu/ucla/cs/zhiyi",
          "ca-info": "/ndn/edu/ucla/cs/www/ca/information.html"
        }

        Example #2
        {
          "identifier": "/ndn/edu/ucla/cs/zhiyi/my-macbook”,
          "ca-info": ""
        }

New certificate application packet exchange
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_NEW interest packet
^^^^^^^^^^^^^^^^^^^^

:Parameters:
  - Command Type :ndn:`_NEW`
  - Certificate request

:MustBeFresh: must be set

:Signature: must be signed by ``user certificate key``

:Verification: When received, several parts need to be verified to ensure authentication:
  - certificate request signature must be verified
  - For certificate renewal and key pair change at same time
    + Valid signature
    + The signature key matches the certificate name
    + The old certificate still not expires

:Naming convention: The interest name format for new certificate and certificate renewal is as follow
  ::

      /CA-prefix/_NEW/<certificate request>/[signature]
                \    /\                    /
                 -  -  --------  ---------
                  \/           \/
           Command Type  Certificate component

:Example:
  - To apply a certificate from /ndn/edu/ucla/ CA, a NEW interest can be::

         /ndn/edu/ucla/_NEW/Bv0DhQdHCANuZG4...xqt5S2+4=/...

Data packet answering _NEW - Verification
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Verification: When received, several parts need to be verified to ensure authentication:
  - The data name is the same with the interest sent
  - CA signature must be verified

:Content format: JSON file format:
  - status: The application status.
  - request-id: The unique ID of the request
  - challenges: A challenge list from which the certificate requester can select one to go to next step
  - challenge
    +  challenge-type: The identifier of the challenge

:JSON example: The example is as follow::
  ::

        Example #1
        {
          "status": "pending",
          "request-id": "1209381203",
          "challenges": [
            {
              "challenge-type": "PIN"
            },
            {
              "challenge-type": "Email"
            }
          ]
        }

Challenge selection and validation packet exchange
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_SELECT interest packet
^^^^^^^^^^^^^^^^^^^^^^^

:Parameters:
  - Command Type :ndn:`_SELECT`
  - Request ID in JSON format
  - Challenge ID
  - Challenge parameters in JSON format (if any)

:MustBeFresh: must be set

:Signature: must be signed by ``user certificate key``

:Verification:
  - Certificate signature must be verified
  - Signature must match the request ID

:Naming convention: The interest name format is as below
  ::

        /CA-prefix/_SELECT/{"Request-ID": "1209381203"}}/<ChallengeID>/{"Param1": "<param1>", ...}/[Signature components]
                  \    /\                           /\                                        /
                   —  -  -------------  ------------  ------------------  --------------------
                    \/                \/                                \/
               Command Type       Request ID          Select a challenge and provide related info

:Example:
  - If /ndn/edu/ucla/ CA module requires user to use email address for challenge, a SELECT interest can be::

         /ndn/edu/ucla/_SELECT/{"request-id":"111"}/EMAIL/{"email-address":"zhiyi@cs.ucla.edu"}/...
                                                    |        |
                                                    v        |
                            Indicates using email challenge  |
                                                             |
                                                             v
                                        User's email address to receive challenge email

  - If /ndn/edu/ucla/cs/zhiyi CA module can accept PIN challenge and PIN challenge require nothing in SELECT, then a SELECT interest can be::

         /ndn/edu/ucla/cs/zhiyi/_SELECT/{"request-id":"111"}/PIN/...
                                                           |
                                                           v
                                 Indicates using PIN code challenge

_VALIDATE interest packet
^^^^^^^^^^^^^^^^^^^^^^^^^

:Parameters:
  - Command Type :ndn:`_VALIDATE`
  - Request ID in JSON format
  - Challenge ID
  - Challenge parameters in JSON format (if any)

:MustBeFresh: must be set

:Signature: must be signed by ``user certificate key``

:Verification: When received, several parts need to be verified to ensure authentication:
  - Certificate signature must be verified
  - Signature must match the request ID

:Naming Convention: The interest name format is as below:
  ::

        /CA-prefix/_VALIDATE/{"request-id": "1209381203"}/<ChallengeID>/{"param1": "<param1>", ...}/[Signature components]
                  \         /\                           /\                                        /
                   --—  ---   -------------  ------------  ------------------  --------------------
                      \/                   \/                                \/
                 Command Type         Request ID          Select a challenge and provide related info


:Example:
  - After sending :ndn:`_SELECT` to select Email challenge, then a following :ndn:`_VALIDATE` interest can be::

         /ndn/edu/ucla/_VALIDATE/{"request-id":"111"}/EMAIL/{"code":"1234"}/...
                                                      |        |
                                                      v        |
                              Indicates using email challenge  |
                                                               |
                                                               v
                                             Verification code obtained from email

  - After sending :ndn:`_SELECT` to select Pin challenge, then a following :ndn:`_VALIDATE` interest can be::

         /ndn/edu/ucla/cs/zhiyi/_VALIDATE/{"Request-ID":"111"}/PIN/{"code":"1234"}/...
                                                                |        |
                                                                v        |
                                      Indicates using PIN code challenge |
                                                                         |
                                                                         v
                                                       The PIN code obtained out of bind

_STATUS interest packet
^^^^^^^^^^^^^^^^^^^^^^^

:Parameters:
  - Command Type :ndn:`_STATUS`
  - Request ID in JSON format
  - Params in JSON format

:MustBeFresh: must be set

:Signature: must be signed by ``user certificate key``

:Verification: When received, several parts need to be verified to ensure authentication:
  - Certificate signature must be verified
  - Signature must match the request ID

:Naming Convention: The interest name format is as below:
  ::

        /CA-prefix/_STATUS/{"request-id": "1209381203"}/[Signature components]
                  \         /\                         /
                   --—  ---   -----------  ------------
                      \/                 \/
                 Command Type         Request ID

:Example:
  - After sending :NDN:`_VALIDATE` to select Email challenge, then a following :NDN:`_VALIDATE` interest can be:
   ::

         /ndn/edu/ucla/_STATUS/{"request-id":"111"}/...

Data packet answering _SELECT, _VALIDATE and _STATUS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Content format: JSON file format:
  - Request-ID: The request ID
  - Challenge-Type: The challenge module type
  - Status: The application status
  - Certificate: The encapsulated certificate signed by CA. This field is used only when challenge is verified successfully

:JSON example: The example is as follow
  ::

        Example #1
        {
          "request-id": "111",
          "challenge-type": "EMAIL",
          "status": "need-email"
        }

        Example #2
        {
          "request-id": "111",
          "challenge-type": "PIN",
          "status": "success",
          "certificate": "/ndn/edu/ucla/CA/_DOWNLOAD/{"Request-ID":"111"}"
        }

Certificate download
~~~~~~~~~~~~~~~~~~~~

_DOWNLOAD interest packet
^^^^^^^^^^^^^^^^^^^^^^^^^

:Parameters:
  - Command Type :ndn:`_DOWNLOAD`
  - Request ID in JSON format

:MustBeFresh: must be set

:Signature: No requirements

:Verification: No requirements

:Naming Convention: The interest name format is as below:
  ::

        /CA-prefix/_DOWNLOAD/{"request-id": "1209381203"}
                  \        /\                           /
                   —--  --   -------------  ------------
                      \/                 \/
               Command Type         Request ID

:Example:
  - After knowing certificate has been issued, a :ndn:`_DOWNLOAD` interest to download the certificate can be:
   ::

         /ndn/edu/ucla/_DOWNLOAD/{"request-id":"111"}

Data packet answering _DOWNLOAD
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Data structure:

   .. aafig::
                                 :aspect: 70
                                 :scale: 100

                                 +--------------------------+
                                 |         "Content"        |
                                 |+------------------------+|
                                 ||       "Cert Data"      ||
                                 |+------------------------+|
                                 +--------------------------+
                                 |      "SignatureInfo"     |
                                 +--------------------------+
                                 |     "SignatureValue"     |
                                 +--------------------------+

Error information
~~~~~~~~~~~~~~~~~

When there is error caused by certificate requester, CA will reply a error JSON within the data:

:Content format: JSON file format:
  - status: The application status. Here it can only be error.
  - error-info: The error information.

:JSON example: The example is as follow::
  ::

        Example #1
        {
          "status": "error",
          "error-info": "invalid poll packet signature"
        }

        Example #2
        {
          "status": "error",
          "error-info": "invalid certificate request in new packet"
        }
