Protocol Overview
-----------------

The following diagram illustrates the main process for CA to perform certificate issuing. There are mainly three processes in certificate application.

Sub namespace assignment
~~~~~~~~~~~~~~~~~~~~~~~~

  The certificate requester first sends an interest called :ndn:`_PROBE` to ask for an available namespace from CA. The CA will reply an available namespace to certificate requester. Notice that whether the certificate requester should perform **sub namespace assignment** step or not depends on CA's policy, which means certificate requester should know CA's policy first. Those CAs for a big number of identities may want to use the sub namespace assignment to avoid name conflicts.

  Requester sends a :ndn:`_PROBE` interest to CA to get itself a namespace. If user wants to get an available namespace, the probe parameter should be set as CA requires. For example, CA may require user to use email address as probe parameter. CA will reply a data packet which contains a JSON file, in which a namespace assigned by CA and available CA information will be included.

  Example:

  .. aafig::
    :aspect: 70
    :scale: 90

    "Certificate requester"                                                   "CA"
             |                                                                 |
             |                    "_PROBE: Probe information"                  |
             | --------------------------------------------------------------> |
             |                                                                "..."
             |                "JSON{namespace, ...}, signed by CA"             |
             | <-------------------------------------------------------------- |
             |                                                                 |

New certificate application
~~~~~~~~~~~~~~~~~~~~~~~~~~~

  With the namespace got from last step, the certificate requester then generate an asymmetric key pairs (user certificate key) with the name and send a NEW interest to CA to apply for a certificate. The NEW interest contains a self-signed certificate (certificate request). When CA receives the NEW command, the CA will collect all available challenge choices according its own policy and reply this challenge list and a request ID to certificate requester.

  Notice that CA will keep certificate requester's request instance at this time point and generate a unique ID for request. After NEW interest, user only need to put this ID to specific interest component and sign the interest with the certificate corresponding key.

  User sends a :ndn:`_NEW` interest to CA to request a new certificate. The interest command will carry a certificate request (self-signed certificate). CA will answer a data packet which contains a JSON file. A set of validation challenge choices is included. Also CA will store the request instance and put a Request-ID to the json file.

  Example:

  .. aafig::
    :aspect: 70
    :scale: 90

    "Certificate requester"                                                  "CA"
            |                                                                 |
            |                "_NEW: cert request, signed by cert"             |
            | --------------------------------------------------------------> |
            |                                                                "..."
            |             "JSON{request id, a set of challenges, ...},"       |
            |                         "signed by CA"                          |
            | <-------------------------------------------------------------- |
            |                                                                 |

Challenge selection and validation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Certificate requester then selects one challenge and notices CA. CA will perform the challenge for requester. Requester would then follow CA's instructions to finish the challenge. CA will query the out-of-band validation results and answer a data which contains the validation status. Once the challenge result is verified by CA, CA will sign the certificate.

  To verify the user's identity, at least one **out-of-band validation** is required. For example, email verification code to the requester, or ask requester to obtain secret code out of bind.

  User sends a :ndn:`_SELECT` **command interest** to CA to inform which challenge have been selected and will be performed. User sends a :ndn:`_VALIDATE` **command interest** to CA to pass required information to CA to finish the challenge. Challenges should take use of the Challenge-Info component in :ndn:`_SELECT` and :ndn:`_VALIDATE`. For example, for email challenge, CA can require user to provide email address in this field. CA will reply user with a data packet which contains a JSON file, in which certificate issuing status and challenge validation status will be included. If the challenge is verified successfully, a certificate download name will be provided too.

  Example:

  .. aafig::
    :aspect: 70
    :scale: 90

    "Certificate requester"                                                  "CA"
            |                                                                 |
            |       "SELECT: JSON{Request-ID}, Challenge-ID, JSON{Params},"   |
            |                       "Signed by cert"                          |
            | --------------------------------------------------------------> |
            |                                                                "..."
            |                 "JSON{status}, Signed by CA"                    |
            | <-------------------------------------------------------------- |
           "..."                                                              |
            |                                                                 |
            |      "VALIDATE: JSON{Request-ID}, Challenge-ID, JSON{Params},"  |
            |                        "Signed by cert"                         |
            | --------------------------------------------------------------> |
            |                                                                "..."
            |                  "JSON{status}, Signed by CA"                   |
            | <-------------------------------------------------------------- |
           "..."                                                              |
            |                                                                 |
            |           "_STATUS: JSON{Request-ID}, Signed by cert"           |
            | --------------------------------------------------------------> |
            |                                                                "..."
            |                  "JSON{status}, Signed by CA"                   |
            | <-------------------------------------------------------------- |

Cerificate download
~~~~~~~~~~~~~~~~~~~

  Once certificate requester knows the certificate has been signed by CA. Requester can fetch the certificate using the name provided in the data from CA.

  Requester sends :ndn:`_DOWNLOAD` to fetch the encapsulated certificate.

Certificate renew is the same as the certificate application. Besides using :ndn:`_NEW` interest to renew certificate, CA can define other strategy for renewal. For root CA like NDN testbed, it requires end entities to use email address for challenge. In this case, NDN testbed CA can use email to notice end entities whose certificates will expire and provide a button for renewal in email, so that there is no need for user to send NEW interest.

Concrete Example
~~~~~~~~~~~~~~~~

As a concrete example, a certificate requester want to apply for a namespace under :ndn:`/ndn/CA`.

1. The certificate requester first select NDN testbed CA module as the target CA module. Once the module is selected, the certificate requester gets CA's policy for certificate application. The policy requires that user should use :ndn:`_PROBE` before NEW and NDN testbed CA only accepts email challenge. In the policy, it is also required that email address should be used as the :ndn:`_PROBE` parameter.

2. Certificate requester first send a :ndn:`_PROBE` interest, which contains its email address :ndn:`zhiyi@cs.ucla.edu`.

3. Then certificate requester would get the data back. After validating the data signature, from data content, certificate requester gets an available namespace :ndn:`/ndn/edu/ucla/zhiyi@cs.ucla.edu`.

4. With the namespace, certificate requester then generates an asymmetric key pair :ndn:`/ndn/edu/ucla/zhiyi@cs.ucla.edu/KEY/[key-id]` and a certificate request (a special formatted self-signed certificate) :ndn:`/ndn/edu/ucla/zhiyi@cs.ucla.edu/KEY/[key-id]/self/[cert-version]` as the certificate request.

5. After that, certificate requester sends the certificate request to NDN testbed CA through :ndn:`_NEW` interest and fetch a Data packet back. After validating the signature of data, requester gets a list of available challenges. In this NDN testbed case, there is only email challenge in the list.

6. Certificate requester then send a :ndn:`_SELECT` interest command noticing CA that Email challenge is selected. In :ndn:`_SELECT` interest, certificate requester puts its email address :ndn:`zhiyi@cs.ucla.edu` to challenge-info part. CA will send a challenge email to this address.  Once certificate requester obtains the verification code from the challenge email, requester then sends a :ndn:`_VALIDATE` command to finish the challenge. :ndn:`_VALIDATE` command carries the verification code and signature from requester.

7. If the status in response for the last :ndn:`_VALIDATE` interest is "pending", certificate requester can periodically send :ndn:`_STATUS` interest to check issuing status.

8. Once the status shows "issued", certificate requester can download the certificate by sending a :ndn:`DOWNLOAD` interest to CA.

  .. aafig::
     :aspect: 70
     :scale: 100

     "certificate requester"                                                  "CA"
             |                           "_PROBE"                              |
             | --------------------------------------------------------------> |
             |                                                   "Get the available namespace"
             |                            "Data"                               |
             | <-------------------------------------------------------------- |
     "Generate key pair"                                                       |
     "Generate certificate request"                                            |
             |                             "_NEW"                              |
             | --------------------------------------------------------------> |
             |                                                  "Generate challenge list"
             |                                         "Store request instance and generate request ID"
             |                             "Data"                              |
             | <-------------------------------------------------------------- |
     "select challenge"                                                        |
             |                            "_SELECT"                            |
             | --------------------------------------------------------------> |
             |                                               "Prepare the challenges for requester"
             |                             "Data"                              |
             | <-------------------------------------------------------------- |
     "Perform Challenge"                                                       |
             |                           "_VALIDATE"                           |
             | --------------------------------------------------------------> |
             |                                                  "Check the challenge result"
             |                                                 "If valid, publish certificate"
             |                                                "In other cases, return the status"
             |                             "Data"                              |
             | <-------------------------------------------------------------- |
             |                                                                 |
                                           "..."
                  "Requester send one or more _VALIDATE to finish challenge."
          "Requester periodically send _STATUS to check status until the cert is issued"
                                           "..."
             |                                                                 |
             |                           "_DOWNLOAD"                           |
             | --------------------------------------------------------------> |
             |                              "Data"                             |
             | <-------------------------------------------------------------- |