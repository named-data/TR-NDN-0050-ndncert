Challenges
----------

This section will introduce how Challenge Module should perform the
challenge and in which way should Challenge Module collaborate with CA.
Also two concrete challenge module examples will be illustrated; these
two modules have been implemented in NDNCERT.

How to perform a challenge
~~~~~~~~~~~~~~~~~~~~~~~~~~

The goal of a challenge is to verify certificate requester's possession
of some resources like an email address or ability to contact directly
with CA. In the main picture of NDNCERT, after certificate requester
send a NEW interest to CA, CA would provide a list of Challenges in
reply. User would then select one challenge type from it and notice CA
in the following :ndn:`_SELECT` interest. Challenge module should take use
one or more :ndn:`_VALIDATE` interest and Data exchange to finish the
challenge.

As mentioned in the section 2.3.1, there is a component called
``ChallengeInfo`` in :ndn:`_VALIDATE` interest name. This ``ChallengeInfo``
is used for certificate requester to provide required information to
Challenge Module. Also there is a JSON field called ``status`` mentioned
in section 2.3.2. This ``status`` is used by Challenge Module to give
instruction to certificate requester.

With interest name component ``ChallengeInfo`` and json field
``status``, Challenge Module can talk directly with certificate
requester. certificate requester have no clue what should be put in
``ChallengeInfo``, so it is Challenge Module who should explicitly
notice certificate requester.

