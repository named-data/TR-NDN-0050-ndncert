Introduction
------------

Every data packet is supposed to have a signature. The signature enables the data consumer to check the integrity and determine whether the data packet should be trusted by verifying the producer.
The signature is usually signed by a certificate and the certificate is issued by CA; thus CA need to verifying an applicant's legality for the certificate.
In common practice, some out-of-band challenges would be used for the purpose.
Given the certificate is widely used in NDN, to facilitate the certificate issuing and management process, NDNCERT is designed.
In NDN, different from TCP/IP world where only well-know node could become CA, every single entity could be a CA.
NDNCERT is also designed not only for well-know node but also for local trust.

NDNCERT could foster simple yet secure NDN certificate issuance and management on NDN testbed, client machines connected to the testbed, and any other computers that speak NDN.
In particular, NDNCERT provides flexible mechanisms to establish certificate authorities (CA) for different NDN namespaces (e.g., NDN Testbed CA for certificates in :ndn:`/ndn namespace`, NDN OpenMHealth authority for :ndn:`/org/openmhealth`, and others) and to request certificates from the established authorities.
Note that the developed NDNCERT protocol does not impose any specific trust model or trust anchors

With NDNCERT, any node can become a certificate authority for a namespace, which is either delegated to this node by a higher-level CA (:ndn:`ndn` -> :ndn:`/ndn/edu/ucla`) or self-claimed (self-signed trust anchor), e.g., when using in local environments such as smart homes.
These hierarchical names are the main contributors to simplicity to request a certificate.
For example, to request a certificate from an NDN Testbed CA, one just needs to send a specially formatted Interest that starts with :ndn:`/ndn/CA`; for certificate from UCLA site of NDN Testbed, send to :ndn:`/ndn/edu/ucla/CA`, etc.
Similarly, to become a CA for :ndn:`/<prefix>`, a node simply needs to start a process that registers :ndn:`/<prefix>/CA` with local NFD.
Note that for the node to become a **recognized** CA, its own certificate needs to be trusted by other parties in the network: implicitly if it is issued by a higher-level authority or explicitly for self-signed certificate.

In NDN any node can take a role of CA, managing certificates in the designated namespace. There are mainly two kinds of CA: CA using a self-signed local trust anchor or a CA using delegated certificate for the namespace.